%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES: 
function animatePendulum(t, states,params)

% Unpack positions
Xq= states(:,1);
Yq = states(:,2);
l = params.l;

numPoints = length(t);

%myVideo = VideoWriter('particlePendulumAnimation');
%myVideo.FrameRate = 100;
%open(myVideo)
animationFigure = figure();
origin = plot(0,0, 'k.','markersize',50);
hold on
linePlot = plot([0;Xq(1)],[0;Yq(1) ], 'k-', 'linewidth',3);
QPlot = plot(Xq(1),Yq(1), 'r.','markersize',20);

axis([-l l -l l]*1.2)
xlabel('x Position [m]');
ylabel('y Position [m]');


for i =1:numPoints
    QPlot.XData = Xq(i);
    QPlot.YData = Yq(i);
    linePlot.XData = [0;Xq(i)];
    linePlot.YData = [0;Yq(i) ];
    drawnow
    %frame = getframe(animationFigure);
     % writeVideo(myVideo,frame);
end
%close(myVideo)



end