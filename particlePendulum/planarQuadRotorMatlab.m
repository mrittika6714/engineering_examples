function [SolutionToAlgebraicEquations,Output] = planarQuadRotorMatlab( TZ, qZ, qZDt, mQ )
if( nargin ~= 4 ) error( 'planarQuadRotorMatlab expects 4 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorMatlab.m created Oct 26 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================
COEF = zeros( 1, 1 );
COEF(1,1) = 1;
RHS = zeros( 1, 1 );
SolutionToAlgebraicEquations(1) = RHS(1) / COEF(1,1);

% Update variables after uncoupling equations
qZDDt = SolutionToAlgebraicEquations(1);



%===========================================================================
Output = zeros( 1, 2 );

Output(1) = qZDt;

Output(2) = qZDDt;

%==============================================
end    % End of function planarQuadRotorMatlab
%==============================================
