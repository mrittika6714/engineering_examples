%% Setup workspace and define System Paramaters
clc; clear; close all
animate = 1;
tStep = 0.01;
tFinal = 10;
tSpan = 0:tStep:tFinal;
params.m = 0.1;
params.l = 1;
params.g = 9.8;
params.b = 0.1;
params.linearized = false;
u0 = [120*pi/180; 0];

%% Uncontrolled Dynamics 
odeFunc = @(t,states) particlePendulumDynamics(t, states, params, 0);
[tUncon, xUncon] = ode45(odeFunc, tSpan, u0);
numPoints = length(tUncon);
extraOutputs=zeros(numPoints, 2);
for i = 1 :  numPoints
    [~,extraOutputs(i,:)] = particlePendulumDynamics(tUncon(i), xUncon(i,:), params, 0);
end
drawStatesUnControlled = extraOutputs(:,:);

%% Linear Controller (linearized about PI)
params.linearized = false;
K =  [ 1.9800    0.6000]; % Use particle pendulum analysis to get this
odeFunc = @(t,states) particlePendulumDynamics(t, states, params, -K*(states-[180*pi/180; 0]));
[tControlled, xControlled] = ode45(odeFunc, tSpan, u0);

numPoints = length(tControlled);
extraOutputs=zeros(numPoints, 2);
for i = 1 :  numPoints
    [~,extraOutputs(i,:)] = particlePendulumDynamics(tControlled(i), xControlled(i,:), params, 0);
end
drawStatesControlled = extraOutputs(:,:);

%% Plot Responses
figure
hold on 
plot(tUncon, xUncon(:,1)*180/pi, 'k', 'linewidth', 2)
plot(tControlled, xControlled(:,1)*180/pi, 'r--', 'linewidth', 2)
% legend('Actual', 'Approx')

%% Animate Responses
if animate ==1
    
animatePendulum(tControlled,drawStatesControlled,params)
animatePendulum(tControlled,drawStatesUnControlled,params)
end


