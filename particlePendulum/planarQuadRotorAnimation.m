%% PROGRAM INFORMATION
%FILENAME: 
%DESCRIPTION: 
%INPUTS: 
%OUTPUTS: 
%AUTHOR: Michal Rittikaidachar
%REVISIION HISTORY:`REV A - x/x/x
%NOTES:
function [Output] = planarQuadRotorAnimation( L, qZ )
if( nargin ~= 2 ) error( 'planarQuadRotorAnimation expects 2 (not %d) command line arguments.', nargin ),  end
%===========================================================================
% File: planarQuadRotorAnimation.m created Oct 26 2021 by MotionGenesis 5.9.
% Portions copyright (c) 2009-2020 Motion Genesis LLC.  Rights reserved.
% MotionGenesis Basic Research (Vanilla) Licensee: Michal Rittikaidachar. (until March 2024).
% Paid-up MotionGenesis Basic Research (Vanilla) licensees are granted the right
% to distribute this code for legal academic (non-professional) purposes only,
% provided this copyright notice appears in all copies and distributions.
%===========================================================================
% The software is provided "as is", without warranty of any kind, express or    
% implied, including but not limited to the warranties of merchantability or    
% fitness for a particular purpose. In no event shall the authors, contributors,
% or copyright holders be liable for any claim, damages or other liability,     
% whether in an action of contract, tort, or otherwise, arising from, out of, or
% in connection with the software or the use or other dealings in the software. 
%===========================================================================





%===========================================================================


%===========================================================================
Output = zeros( 1, 2 );

Output(1) = L*sin(qZ);
Output(2) = -L*cos(qZ);

%=================================================
end    % End of function planarQuadRotorAnimation
%=================================================
