function control = particlePendulumController(t, states, setPoint, params)

mode = params.mode;


if strcmp(mode, 'none')
    control = 0;
    
elseif strcmp(mode, 'constant')
   control = params.value;

elseif strcmp(mode, 'sin')
   control = sin(t);

elseif strcmp(mode, 'linear')
    K = params.value;
    control = -K*(states-setPoint);
   
end    
end