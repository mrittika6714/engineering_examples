%% Analysis of the paricle pendulum 

% Define System
syms theta thetaDot m b l T g real
Teq  = 0 %Equilibrium torque
xDot= [ thetaDot; (1/(m*l^2)) * T - (g/l) * sin(theta) - b/m * thetaDot ]

% Perform analysis of linearized system
Df=simplify(jacobian(xDot, [theta thetaDot]))
EqPoints = solve ( subs(xDot,T, 0)  == 0, theta, thetaDot,'ReturnConditions', true)

A1 = subs(Df, {theta, thetaDot}, {0, 0})
A2 =subs(Df, {theta, thetaDot}, {pi, 0})
A1Sub =double(subs(A1, {m, b, l, g }, {0.1, 0.1, 1, 9.8}))
A2Sub = double(subs(A2, {m, b, l, g }, {0.1, 0.1, 1, 9.8}))

eigenvalues=expand(eig(A1))
eigenvalues=expand(eig(A2))
% note stabiility/instability of eigenvalyes 
eig1 = eig(A1Sub)
eig2 = eig(A2Sub)

B = [ 0; 1/(m*l^2)]
BSub = double(subs(B, { m, l}, {0.1, 1}))
rank1 = rank(ctrb(A1Sub,BSub))
rank2 = rank(ctrb(A2Sub,BSub))

% Ranks are 2 therefore sstem is controllable


% Place poles of the closed loop system
eigsDesired = [-5; -2] 

k = place(A2Sub, BSub, eigsDesired)

test = eig(A2Sub - BSub*k)