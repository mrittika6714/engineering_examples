function [statesDot, extraOutputs] = particlePendulumDynamics(t, states, params,input)
% qDotDot +cxDot+kx =mg
% States = [ theta, thetaDot]

% ToDo Matrix form of state space equation
m = params.m;
g = params.g;
l = params.l;
b = params.b;
linearized = params.linearized;

T = input;

theta = states(1);
thetaDot = states(2);

if linearized == true
   x1 = params.linPoint(1);
   x2 = params.linPoint(2);
   
   
   A = [ 0, 1; -(g/l)*cos(x1), -b/m]
   B = [ 0; 1/(m*l^2)];
   u = [T]
   
    xBarDot = A * states;% + B * u;
    
    statesDot = xBarDot;
else
    xDot = states(2);
    xDotDot = 1/(m*l^2) * T - (g/l) * sin(theta) - b/m * thetaDot;
    statesDot = [xDot; xDotDot];
end

extraOutputs = planarQuadRotorAnimation( l, theta);

end