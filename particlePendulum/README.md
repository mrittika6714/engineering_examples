# Particle Pendulum

A simple particle pendulum with viscous damping is analyzed. 

## Getting started


## Model

### Kinematics
$\vec{r}^{A/N_o} = -y \hat{a}_y$  
${}^{N}\vec{v}^{A_{cm}} = y\dot{\theta} \hat{a}_x$  
${}^{N}\vec{a}^{A_{cm}} = y\ddot{\theta} \hat{a}_y$  


### Dynamics

Newtons Equation  
$\vec{F}^{A} = -mg\hat{n}_y - bl \dot{\theta} \hat{a}_x \rightrightarrows$  

Eulers Equation:   
$\vec{M}^{A/N_o} =  \vec{\vec{I}}{^{A/N_o}}\cdot {}^N\vec{\alpha}^A  + {}^N\vec{\omega}^A \times \vec{\vec{I}}{^{A/N_o}}\cdot {}^N\vec{\omega}^A + m^A\vec{r}^{A/N_o}\times {}^N\vec{a}^{N_o}$

$\vec{r}^{A/N_o}\times \vec{F}^{A}  $

$\tau_z{n}_z - mgl*sin(\theta)- bl^2 \dot{\theta}$  
$\vec{\Tau}^{A/N} =\tau_z{n}_z - mgl*sin(\theta)- bl^2 \dot{\theta}$  
$\vec{\Tau}^{A/N} = ...$  
$\vec{M}^{A/N_o} \cdot \hat{a}_z = ... \cdot \hat{a}_z$  
$ \tau_z -mglsin{\theta} - bl^2\dot{\theta} = ml^2 \ddot{\theta}$ 

Solving tor $\ddot{\theta}$

$\ddot{\theta} = \frac{1}{ml^2}(\tau_z -mglsin{\theta} - bl^2\dot{\theta})$

$\ddot{\theta} = \frac{1}{ml^2}(\tau_z) -\frac{g}{l}sin{\theta} - \frac{b}{m}\dot{\theta})$


in form $\dot{x} = f(x)$
$$ \begin{bmatrix} 
\dot{x_1} \\ 
\dot{x_2} \\
\end{bmatrix} 
 = 
 \begin{bmatrix} 
 {x_2} \\ 
\frac{1}{ml^2}(\tau_z) -\frac{g}{l}sin{\theta} - \frac{b}{m}\dot{\theta}) \\
\end{bmatrix} 
$$


The equailibrium points of the system are found by setting $f = \dot{x_1} = \dot{x_2} = 0 $ and solving for $x_1$ and $x_2$  

we have equilibrium points at 

$$
\begin{bmatrix} 
x_1\\ 
x_2  \\
\end{bmatrix} = 
 \begin{bmatrix} 
0 \\ 
0 \\
\end{bmatrix}, 
\begin{bmatrix} 
n\pi\\ 
0  \\
\end{bmatrix}
$$

Taking the jacobian we get

$$
J = \begin{bmatrix} 
0 & 1 \\ 
-\frac{g}{l}cos(\theta) & -\frac{b}{m} \\
\end{bmatrix} 
$$


Evaluating the jacobian at our equilibrium points we get:


$$
\begin{bmatrix}
0 & 1 \\
-\frac{g}{l} & -\frac{b}{m}
\end{bmatrix},
\begin{bmatrix}
0 & 1 \\
\frac{g}{l} & -\frac{b}{m}
\end{bmatrix}
$$


Here we see the eigenvalues of the linearized system depend on the damping, mass, and length of the system. (Example 4.15 of khalil)

